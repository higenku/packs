#!python

import sys, os, pathlib
import subprocess

def main():
    if len(sys.argv) < 2:
        print("no Command specified")

    elif sys.argv[1] == "pack":
        print("Packing dependencies")
        libraries = os.listdir("./Packs")
        print(f"Total libs {len(libraries)}")
        for i in libraries:
            # print(f"building {i}")
            proc = subprocess.Popen(["python3", "-u", f"Packs/{i}/x.py"])

            proc.wait()

    elif sys.argv[1] == "push":
        print("Not implemented")

if __name__ == "__main__":
    main()