# Compression

Instead of worring about wich libraries you need for compression and package it with your application, just add this dependency

Libraries:
- [ ] Bzip2
- [ ] Gzip
- [ ] Tar
- [ ] zip
- [ ] xz
- [ ] 7z
- [ ] zstd
- [ ] zlib
- [ ] brotoli
