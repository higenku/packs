# Rust beta channel
rust beta channel releases a new release each six weeks as described [here](https://github.com/rust-lang/rfcs/blob/master/text/0507-release-channels.md#exceptions-for-the-100-beta-period) 

## Supported Versions
as there is new version every six weeks, for now, we will only support beta and stable