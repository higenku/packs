Name: dotnet3.1 
Version: 3.1.11
License: Mit
Dependencies: []
Targets: 
  
  - # linux package
    Sys: "linux-x64"
    PackegeDir: "tmp/lin"

  - # windows package
    Sys: "win-x64"
    PackegeDir: "tmp/win"
    
  - # Macos package
    Sys: "osx-x64"
    PackegeDir: "tmp/osx"