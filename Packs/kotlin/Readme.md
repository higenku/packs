# Kotlin dependencies

Releases from https://github.com/JetBrains/kotlin/releases/.

We remove the unecessary binaries like the compiler kotlinc, 

more specifically, we download https://github.com/JetBrains/kotlin/releases/download/{version}/kotlin-compiler-{version}.zip and include in the final package:
- lib/
- license/
- bin/kotlin
