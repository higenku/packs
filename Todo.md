# Todo
priorize clang packages, then mingw.
look at https://github.com/NixOS/nixpkgs/tree/nixos-unstable/pkgs/development

- [ ] Add Support for osx:
    - [ ] x64
    - [ ] aarch64 (arm64)

- [ ] Add Suport for win:
    - [ ] x64
    - msys2 repos:
        - [ ] MINGW64
        - [ ] UCRT64
        - [ ] CLANG64 (later)

- [ ] Add Support for lin:
    - [ ] x64
    - [ ] aarch64 (arm64)
    - repos:
        - [ ] Core
        - [ ] Extra
        - [ ] Community
- [ ] Add [DEPENDECY].json to the Releases foder containing information about the depencies, among other things, name, a list with arch, system, download link, etc

- [ ] Gtk
    - [ ] 2
    - [ ] 3
    - [ ] 4
- [ ] Gtkmm
    - [ ] 2
    - [ ] 3
    - [ ] 4
- [ ] Qt5
    - [ ] breeze icons qt5
- [ ] GStreamer
- [ ] Compression
- [ ] image
- [ ] node
- [ ] electron
- [ ] dotnet
- [ ] glib and glibc
- [ ] llvm / mingw standart libraries
- [ ] asio
- [ ] aspell
- [ ] crypto
- [ ] crypto++
- [ ] brotli
- [ ] ca-certificates
- [ ] cjson
- [ ] confuse
- [ ] connect
- [ ] dbus
- [ ] ffms2
- [ ] pixbuf2
- [ ] gspell
- [ ] libav
- [ ] hidapi
- [ ] http-parser
- [ ] freetype
- [ ] ffmpeg
- [ ] 

## C Standart Libraries
- [ ] glib2


## C++ Standart
- [ ] glib2mm
- [ ] libc++
    - [ ] brew
    - [ ] linux
    - [ ] mingw
    - [ ] clang
    - [ ] ucrt
